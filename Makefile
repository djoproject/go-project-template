.PHONY : all clean test doc
.SILENT: clean

all: cmd/main/main

doc: docs/README.pdf

cmd/main/main: cmd/main/main.go
	go build -o cmd/main/main cmd/main/main.go

docs/README.pdf: README.md
	pandoc README.md -o docs/README.pdf

clean:
	rm -f cmd/main/main

test:
	go test -v ./...
