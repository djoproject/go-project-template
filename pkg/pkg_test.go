package pkg_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go_project_template/pkg"
)

func TestWorld(t *testing.T) {
	assert.Equal(t, "World", pkg.World())
}
