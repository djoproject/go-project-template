package internal_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go_project_template/internal"
)

func TestHello(t *testing.T) {
	assert.Equal(t, "Hello", internal.Hello())
}
