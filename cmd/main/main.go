package main

import (
	"fmt"

	"gitlab.com/go_project_template/internal"
	"gitlab.com/go_project_template/pkg"
)

func main() {
	fmt.Printf("%s %s !\n", internal.Hello(), pkg.World())
}
