# My amazing Golang project

## Native execution

### To run unit tests

```bash
make test
```

### To compile

```bash
make
```

### To execute

```bash
./cmd/main/main
```

## Docker execution

### Creating a docker image

```bash
make
docker build --file build/package/Dockerfile -t test .
```

### Running the docker image

```bash
docker run -it --rm test
```

## Documentation
* [Dockerfile](https://docs.docker.com/engine/reference/builder/)
* [Folders structure](https://github.com/golang-standards/project-layout)
